﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextLifes : MonoBehaviour
{
    public int heart;
    void Update()
    {
        if (heart == 1 && GameManager.Instance.Lifes==0)
        {
            Destroy(gameObject);
        }
        if (heart ==0 && GameManager.Instance.Lifes == -1)
        {
            Destroy(gameObject);
        }
    }
}
