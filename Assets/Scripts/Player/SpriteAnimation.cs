﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class SpriteAnimation : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    private int animationSpritesIndex;
    private Sprite[] animationSprite;
    private int change;
    private float framerate;
    private int AnimationFR;
    private int Frame2change;
    private int counterframe;
    // Start is called before the first frame update
    void Start()
    {
        
        animationSprite = gameObject.GetComponent<DataPlayer>().Sprites;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        
        spriteRenderer.sprite = animationSprite[1];
        animationSpritesIndex = 0;
        change = 0;
        counterframe = 0;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        framerate = 1 / Time.deltaTime;
        AnimationFR = 3 * animationSprite.Length;
        Frame2change = Mathf.RoundToInt(framerate) / AnimationFR;
        counterframe++;
        if (GameManager.Instance.GetComponent<Animation>() == true)
        {
            if (counterframe % Frame2change == 0)
            {
                spriteRenderer.sprite = animationSprite[animationSpritesIndex];
                animationSpritesIndex++;

                if (animationSpritesIndex == 4)
                {
                    animationSpritesIndex = 0;
                }
            }
        }
        else
        {
            spriteRenderer.sprite = animationSprite[0];
        }
    }

}
