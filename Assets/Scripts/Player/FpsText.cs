﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FpsText : MonoBehaviour
{
    private float fps;
    public Text fpsDisplay;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        fps = 1 / Time.deltaTime;
 
        fpsDisplay.text = Mathf.RoundToInt(fps) + " FPS";
    }
}
