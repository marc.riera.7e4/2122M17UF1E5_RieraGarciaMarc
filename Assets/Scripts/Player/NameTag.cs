﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NameTag : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI nameText;
    // Start is called before the first frame update
    void Start()
    {
        SetName();
    }

    private void SetName()
    {
        nameText.text = gameObject.GetComponent<DataPlayer>().Name + " " + gameObject.GetComponent<DataPlayer>().Surname + ": " + gameObject.GetComponent<DataPlayer>().PKind;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
