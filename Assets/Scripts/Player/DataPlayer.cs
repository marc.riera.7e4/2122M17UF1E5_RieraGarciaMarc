﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerKind
{
    Sorcerer,
    Knight,
    Assassin,
    Bard,
    Barbarian
}


public class DataPlayer : MonoBehaviour
{
    [SerializeField]
    public string Name = "Robertoo";
    public string Surname;
    public float Speed = 1f;
    public float Height = 5;
    public float Weight = 1;
    public float WalkDistance = 10;
    public int lifes;

    public PlayerKind PKind;
    
    public Sprite[] Sprites;


    void Start()
    {
        transform.localScale = new Vector3(gameObject.GetComponent<DataPlayer>().Height, gameObject.GetComponent<DataPlayer>().Height, 1);
        /*switch (currentState)
        {
            case ground:
                growing();
            case giantState:

        }
        */
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(1/Time.deltaTime);


    }
   

}
