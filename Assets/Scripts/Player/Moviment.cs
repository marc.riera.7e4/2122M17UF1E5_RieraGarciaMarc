﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moviment : MonoBehaviour
{
    public SpriteRenderer Sp;
    public DataPlayer Dp;
    private bool right;
    private int distance;
    // Start is called before the first frame update
    void Start()
    {
        Sp = gameObject.GetComponent<SpriteRenderer>();
        right = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (right == false)
        {
            transform.position += Vector3.left * gameObject.GetComponent<DataPlayer>().Speed * Time.deltaTime / gameObject.GetComponent<DataPlayer>().Weight;
            Sp.flipX = false;
            if (transform.position.x <= -6)
            {
                right = true;
            }
        } 

        if (right == true)
        {
            transform.position += Vector3.right * gameObject.GetComponent<DataPlayer>().Speed * Time.deltaTime / gameObject.GetComponent<DataPlayer>().Weight;
            Sp.flipX = true;
            if (transform.position.x >= 6)
            {
                right = false;
                
            }
        }




        if (Input.GetKey(KeyCode.W))
        {
            transform.position += Vector3.up * gameObject.GetComponent<DataPlayer>().Speed * Time.deltaTime / gameObject.GetComponent<DataPlayer>().Weight;
            
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position += Vector3.down * gameObject.GetComponent<DataPlayer>().Speed * Time.deltaTime / gameObject.GetComponent<DataPlayer>().Weight;
            
        }
    }
}
