﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiantAbility : MonoBehaviour
{
    private bool active;
    private bool cooldown;
    private bool disactive;
    private float x;
    private float y;
    private float height;
    // Update is called once per frame
    void Start()
    { 
        height = gameObject.GetComponent<DataPlayer>().Height;
        x = height;
        y = height;
        disactive = false;
        cooldown = false;        
    }

    void Update()
    {

        if (Input.GetKey(KeyCode.Space) && height == transform.localScale.x && cooldown == false)
        {
            active = true;
            cooldown = true;
        }
        if (active == true)
        {
            x = x + 2 * Time.deltaTime * height;
            y = y + 2 * Time.deltaTime * height;
            transform.localScale = new Vector3(x, y, 1);

            if (transform.localScale.x >= 3 * height && transform.localScale.y >= 3 * height)
            {
                active = false;
                StartCoroutine(waiterAbility());
            }
        }
        if(disactive == true){
            x = x - 2 * Time.deltaTime * height;
            y = y - 2 * Time.deltaTime * height;
            transform.localScale = new Vector3(x, y, 1);

            if (transform.localScale.x <= height && transform.localScale.y <= height)
            {
                transform.localScale = new Vector3(height, height, 1);
                StartCoroutine(waiterCd());
                disactive = false;
            }
        }
    }
    IEnumerator waiterAbility()
    {
        yield return new WaitForSeconds(3);
        disactive = true;

    }

    IEnumerator waiterCd()
    {
        yield return new WaitForSeconds(4);
        cooldown = false;
    }


}
