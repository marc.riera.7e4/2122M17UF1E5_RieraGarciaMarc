﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    public int Score;

    public int EnemiesInGame;

    public int Lifes;

    private GameObject player;
    private UIController ui;
    [SerializeField]
    public bool isGround;
    private void Awake()
    {
        if (_instance == null)
        {
            
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        Score = 0;
        EnemiesInGame = 0;
        player = GameObject.Find("Player");
        Lifes = player.GetComponent<DataPlayer>().lifes;
        ui = GameObject.Find("Canvas").GetComponent<UIController>();
    }

    // Update is called once per frame
    void Update()
    {

        EnemiesInGame = GameObject.FindGameObjectsWithTag("Enemic").Length;
    }

    public void increaseScore (int score)
    {
        Debug.Log("score");
        Score = Score + score;
    }
    public void reduceLifes()
    {
        Debug.Log("life");
        Lifes--;
    }
    public void restart()
    {
        new WaitForSeconds(3);

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        
    }
}
