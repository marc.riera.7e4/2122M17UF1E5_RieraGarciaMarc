﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGround : MonoBehaviour
{
    Rigidbody2D rb2D;


    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        
    }
    private void FixedUpdate()
    {
        Debug.Log(GameManager.Instance.isGround);
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        GameManager.Instance.isGround = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameManager.Instance.isGround = true;
    }
 
    private void OnTriggerExit2D(Collider2D collision)
    {
        GameManager.Instance.isGround = false;
    }
}
