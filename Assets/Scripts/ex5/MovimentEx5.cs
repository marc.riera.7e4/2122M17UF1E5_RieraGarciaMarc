﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimentEx5 : MonoBehaviour
{
    SpriteRenderer Sp;
    private bool right;
    Rigidbody2D rb2D;
    private void Start()
    {
        Sp = gameObject.GetComponent<SpriteRenderer>();
        rb2D = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Vector3 m_Input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
      
        if (Input.GetKey(KeyCode.W) &&  GameManager.Instance.isGround == true)
        {
            rb2D.AddForce(new Vector2(0, 300f));
            GameManager.Instance.isGround = false;
            Debug.Log("salt");
        }

        if (Input.GetKey(KeyCode.A))
        {
            rb2D.AddForce(new Vector2(-600f*Time.deltaTime,0));
            Sp.flipX = true;
            Debug.Log("a");
        }
        if (Input.GetKey(KeyCode.D))
        {
            rb2D.AddForce(new Vector2(600f * Time.deltaTime, 0));
            Sp.flipX = false;
            Debug.Log("d");
        }
    }
    
}
    

