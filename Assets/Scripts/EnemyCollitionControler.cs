﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollitionControler : MonoBehaviour
{
    [SerializeField]
    public int score;
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("Player"))
        {
            Debug.Log("Colisio amb player");
            Destroy(gameObject);
            GameObject pl = collision.gameObject;
            int pllifes = pl.GetComponent<DataPlayer>().lifes;

            if (GameManager.Instance.Lifes <= 0)
            {
                GameManager.Instance.reduceLifes();
                Destroy(pl);
                GameManager.Instance.restart();

            }
            else
            {
                GameManager.Instance.reduceLifes();
            }

        }
        else
        {
            Destroy(gameObject);
            GameManager.Instance.increaseScore(score);
        }
    }
}
